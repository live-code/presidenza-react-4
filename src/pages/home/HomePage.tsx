import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { setConfig, setTheme } from '../../core/store/config.actions';
import { addNews, deleteNews, toggleNews } from './store/news.store';

export const HomePage: React.VFC = () => {
  const newsList = useSelector((state: RootState) => state.news)
  const dispatch = useDispatch();

  function onChange(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value));
      e.currentTarget.value = '';
    }
  }

  return <div>
    <h1>News (sync store)</h1>
    <input type="text" onKeyPress={onChange} placeholder="News title (confirm with Enter)"/>

    {
      newsList.map(news => {
        return <li className="list-group-item" key={news.id}>
          {news.title}
          <i className="fa fa-trash"
             onClick={() => dispatch(deleteNews(news.id))}/>
          <div
            className="badge badge-dark"
            onClick={() => dispatch(toggleNews(news.id))}
          >
            {news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
          </div>
        </li>
      })
    }

    <h1>Setting</h1>

    <button onClick={() => dispatch(setTheme('dark'))}>dark</button>
    <button onClick={() => dispatch(setTheme('light'))}>light</button>
  </div>
};
