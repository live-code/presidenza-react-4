export interface Config {
  itemsPerPallet: number;
  material: string;
  theme: 'dark' | 'light'
}
