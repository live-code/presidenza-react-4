import { RootState } from '../../../../App';
import { Product } from '../../model/product';

export const selectProductsList = (state: RootState) => state.catalog.products.list;
export const selectProductsError = (state: RootState) => state.catalog.products.error;

export const selectProductsListByCat = (state: RootState) => {
  const catId = state.catalog.categories.active;
  return state.catalog.products.list
    .filter(p => catId === -1 ? true : p.categoryID === catId);

}

export const selectProductsTotal = (state: RootState) => state.catalog.products.list.reduce((acc: number, cur: Product) => {
  return acc + cur.price;
}, 0)
