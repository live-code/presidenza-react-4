import { createAction } from '@reduxjs/toolkit';

export const openLeftPanel = createAction('ui/openLeftPanel')
export const openRightPanel = createAction('ui/openRightPanel')
export const closeLeftPanel = createAction('ui/closeLeftPanel')
export const closeRightPanel = createAction('ui/closeRightPanel')
