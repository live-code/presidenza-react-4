import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../model/news';

export const newsStore = createSlice({
  name: 'users',
  initialState: [] as News[],
  reducers: {
    addNews(state, action: PayloadAction<string>) {
      state.push({
        id: Date.now(),
        title: action.payload,
        published: false
      });
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.findIndex(news => news.id === action.payload)
      state.splice(index, 1)
    },
    toggleNews(state, action: PayloadAction<number>) {
      const news = state.find(news => news.id === action.payload)
      if (news) {
        news.published = !news.published;
      }
    }
  }
})

export const { addNews, deleteNews, toggleNews } = newsStore.actions
