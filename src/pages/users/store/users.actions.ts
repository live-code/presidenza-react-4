import { Action, createAction } from '@reduxjs/toolkit';
import { User } from '../model/users';
import { AppThunk, RootState } from '../../../App';
import axios from 'axios';
import { Dispatch } from 'react';

export const loadUsers = (): AppThunk => (dispatch: Dispatch<Action>, getStore: () => RootState) => {
  console.log('get store', getStore())
  axios.get<User[]>('http://localhost:3001/users')
    .then(res => dispatch(loadUsersSuccess(res.data)))
    .catch(err => dispatch(loadUsersFailed()))
}

export const loadUsersSuccess = createAction<User[]>('users/loadUsersSuccess');
export const loadUsersFailed = createAction('users/loadUsersFailed');


export const addUser = (user: Omit<User, 'id'>): AppThunk => (dispatch: Dispatch<Action>) => {
  axios.post<User>('http://localhost:3001/users', user)
    .then(res => dispatch(addUserSuccess(res.data)))
    .catch(err => dispatch(addUserFailed()))
}

export const addUserSuccess = createAction<User>('users/addSuccess');
export const addUserFailed = createAction('users/addFailed');


export const deleteUser = (id: number): AppThunk => (dispatch: Dispatch<Action>) => {
  axios.delete('http://localhost:3001/users/' + id)
    .then(res => dispatch(deleteUserSuccess(id)))
    .catch(err => dispatch(deleteUserFailed()))
}
export const deleteUserSuccess = createAction<number>('users/deleteSuccess');
export const deleteUserFailed = createAction('users/deleteFailed');



export const setAsAdmin = (user: User): AppThunk => (dispatch: Dispatch<Action>) => {
  axios.patch<User>('http://localhost:3001/users/' + user.id, {
    isAdmin: !user.isAdmin
  })
    .then(res => dispatch(setAsAdminSuccess(user)))
    .catch(err => dispatch(setAsAdminFailed()))
}
export const setAsAdminSuccess = createAction<User>('users/setUserAsAdminSuccess');
export const setAsAdminFailed = createAction('users/setUserAsAdminFailed');
