import React  from 'react';
import { Product } from '../model/product';
import { deleteProduct, toggleProduct } from '../store/products/products.actions';

export interface ProductsListProps {
  products: Product[];
  total: number;
  onDeleteProduct: (id: number) => void;
  onToggleVisibility: (product: Product) => void;
}
export const ProductsList: React.VFC<ProductsListProps> = (props) => {
  const { products, total, onDeleteProduct, onToggleVisibility } = props;

  const deleteHandler = (event: React.MouseEvent<HTMLElement>, id: number) => {
    event.stopPropagation();
    onDeleteProduct(id);
  };

  return <>
    {
      products.map((product: Product) => {
        {/*NEW: style visibility*/}
        return (
          <li
            className="list-group-item"
            style={{ opacity: product.visibility ? 1 : 0.5}}
            key={product.id}
          >
            {/*NEW*/}
            <span onClick={() => onToggleVisibility(product)}>
              {
                product.visibility ?
                  <i className="fa fa-eye" /> :
                  <i className="fa fa-eye-slash" />
              }
            </span>
            <span className="ml-2">{ product.title }</span>

            {/*NEW*/}
            <div className="pull-right">
              { product.price }
              <i
                className="fa fa-trash "
                onClick={(e) => deleteHandler(e, product.id)}
              />
            </div>

          </li>
        )
      })
    }
    {/*NEW*/}
    <div className="text-center">
      <div className="badge badge-info">Total: € {total}</div>
    </div>
  </>
};
