import { createReducer } from '@reduxjs/toolkit';
import { setConfig, setTheme, setThemeSuccess } from './config.actions';
import { Config } from './config';

const INITIAL_STATE: Config = {
  itemsPerPallet: 12,
  material: 'wood',
  theme: 'light'
}

export const configReducer = createReducer(INITIAL_STATE, {
  [setConfig.type]: (state, action) => ({ ...state, ...action.payload }),
  [setThemeSuccess.type]: (state, action) => ({ ...state, theme: action.payload })
})

