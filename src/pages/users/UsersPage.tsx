import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addUser, deleteUser, loadUsers, setAsAdmin } from './store/users.actions';
import { getUsers } from './store/users.selectors';
import css from './UserPage.module.css';
import cn from 'classnames';
import { getLeftPanelOpenStatus, getRightPanelOpenStatus } from './store/ui.selectors';
import { closeLeftPanel, closeRightPanel, openLeftPanel, openRightPanel } from './store/ui.actions';
import { MyButton, SidePanel } from './components/SidePanel';

export const UsersPage: React.VFC = () => {
  const users = useSelector(getUsers);
  const isLeftOpen = useSelector(getLeftPanelOpenStatus);
  const isRightOpen = useSelector(getRightPanelOpenStatus);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadUsers())
  }, []);

  function addUserHandler() {
    dispatch(addUser({
      name: 'User ' + Date.now(),
      isAdmin: false
    }))
  }
  return <div>

    <MyButton
      disabled={true}
      onClick={() => console.log('ciao')}>CLICK ME</MyButton>

    <SidePanel
      style={{ background: 'red' }}
      onMouseOver={() => console.log('click')}
      position="left" onClose={() => dispatch(closeLeftPanel())} opened={isLeftOpen}>
      bla bla left
    </SidePanel>

    <SidePanel position="right" onClose={() => dispatch(closeRightPanel())} opened={isRightOpen}>
      bla bla right
    </SidePanel>

    <div>TOTAL USERS: { users.length }</div>
    <button onClick={addUserHandler}>ADD USER</button>
    <button disabled={isLeftOpen}
      onClick={() => dispatch(openLeftPanel())}>OPEN LEFT PANEL</button>
    <button
      disabled={isRightOpen}
      onClick={() => dispatch(openRightPanel())}>OPEN RIGHT PANEL</button>

    {isRightOpen && <button>x</button>}
    {
      users.map(u => {
        return <li className="list-group-item" key={u.id}>
          {u.name}

          <i
            onClick={() => dispatch(deleteUser(u.id))}
            className="fa fa-trash" />

          <span onClick={() => dispatch(setAsAdmin(u))}>
          {
            u.isAdmin ?
              <i className="fa fa-check" /> :
              <i className="fa fa-square-o" />
          }
          </span>
        </li>
      })
    }
  </div>
};
