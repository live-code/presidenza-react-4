import { RootState } from '../../App';

export const getConfig = (state: RootState) => state.config;

export const getMaterial = (state: RootState) => state.config.material;
export const getItemsPerPallet = (state: RootState) => state.config.itemsPerPallet;
export const getTheme = (state: RootState) => state.config.theme;
