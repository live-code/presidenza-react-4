import React  from 'react';
import cn from 'classnames';
import css from '../UserPage.module.css';

interface SidePanelProps {
  onClose: () => void;
  opened: boolean;
  position: 'left' | 'right';
}
export const SidePanel: React.FC<SidePanelProps & React.HTMLAttributes<HTMLDivElement>> = ({
  onClose, opened, position, children, ...rest
}) => {
  return <div {...rest} className={cn(
    css.sidepanel,
    position === 'left' ? css.left : css.right,
    { [css.leftOpen]: opened && position === 'left' },
    { [css.rightOpen]: opened && position === 'right' },
    )
  }>
    {children}
    <button onClick={onClose}>close</button>
  </div>
};


export const MyButton: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = (props) => {
  return <button className="btn btn-primary" {...props}>ciao</button>
}
