import Axios from 'axios';

import { AppThunk } from '../../../../App';
import { Product } from '../../model/product';
import { addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess, getProductsFailed, addProductsFailed, deleteProductsFailed, toggleProductsFailed } from './products.store';

export const getProducts = (): AppThunk => async dispatch => {
    try {
      const response = await Axios.get<Product[]>('http://localhost:3001/products')
      dispatch(getProductsSuccess(response.data))
    } catch (err) {
      dispatch(getProductsFailed())
    }
}


export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await Axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    dispatch(deleteProductsFailed())
  }
};

export const addProduct = (
  product: Pick<Product, 'title' | 'price'>
): AppThunk => async dispatch => {

  try {
    const newProduct = await Axios.post<Product>('http://localhost:3001/products', {
        ...product,
        visibility: false
      }
    );
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    dispatch(addProductsFailed())
  }
};

export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {

    const response = await Axios.patch<Product>(`http://localhost:3001/products/${product.id}`, {
      // ...product,
      visibility: !product.visibility
    });
    dispatch(toggleProductVisibility(response.data))
  } catch (err) {
    dispatch(toggleProductsFailed())
  }
};
