import { createReducer } from '@reduxjs/toolkit';
import { closeLeftPanel, closeRightPanel, openLeftPanel, openRightPanel } from './ui.actions';

interface UiState {
  isLeftOpen: boolean;
  isRightOpen: boolean;
}

const initialState: UiState = {
  isLeftOpen: false,
  isRightOpen: false,
}
export const uiReducer = createReducer(initialState, {
  [openLeftPanel.type]: state => {
    state.isRightOpen = false;
    state.isLeftOpen = true;
  },
  [closeLeftPanel.type]: state => {
    state.isLeftOpen = false;
  },
  [closeRightPanel.type]: state => {
    state.isRightOpen = false;
  },
  [openRightPanel.type]: state => ({isLeftOpen: false, isRightOpen: true})
})
