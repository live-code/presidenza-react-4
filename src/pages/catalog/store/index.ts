import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products/products.store';
import { categoriesStore } from './categories/categories.store';

export const catalogReducers = combineReducers({
  products: productsStore.reducer,
  categories: categoriesStore.reducer
})
