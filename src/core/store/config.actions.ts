import { createAction } from '@reduxjs/toolkit';
import { Config } from './config';
import { AppThunk } from '../../App';
import axios from 'axios';

export const setConfig = createAction<Partial<Config>>('set config')

export const loadTheme = (): AppThunk => (dispatch: any) => {
  axios.get<Config>('http://localhost:3001/config')
    .then(res => dispatch(loadThemeSuccess(res.data.theme)))
}
export const setTheme = (theme: 'dark' | 'light'): AppThunk => (dispatch: any) => {
  axios.patch('http://localhost:3001/config', { theme })
    .then(res => dispatch(setThemeSuccess(theme)))
}
export const loadThemeSuccess = createAction<'dark' | 'light'>('set theme')
export const setThemeSuccess = createAction<'dark' | 'light'>('set theme')
