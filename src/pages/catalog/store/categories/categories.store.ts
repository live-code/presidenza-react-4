import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../../../App';

export const categoriesStore = createSlice({
  name: 'categories',
  initialState: {
    list: [
      { id: -1, name: 'All'},
      { id: 1, name: 'Cioccolato'},
      { id: 2, name: 'Latticini'},
    ],
    active: -1
  },
  reducers: {
    changeActive(state, action: PayloadAction<number>) {
      state.active = action.payload;
      // return  ({...state, active: action.payload})
    }
  }
})

export const { changeActive } = categoriesStore.actions;

export const selectCategories = (state: RootState) => state.catalog.categories.list
export const selectCategoriesActive = (state: RootState) => state.catalog.categories.active
