import { RootState } from '../../../App';
export const getCounter = (state: RootState) => state.counter;
export const getTotalPallet = (state: RootState) => Math.ceil(state.counter / state.config.itemsPerPallet);

