import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products/products.actions';
import {
  selectProductsError,
  selectProductsListByCat,
  selectProductsTotal
} from './store/products/products.selectors';
import { ProductsList } from './components/ProductsList';
import { ProductsForm } from './components/ProductsForm';
import { changeActive, selectCategories, selectCategoriesActive } from './store/categories/categories.store';
import cn from 'classnames';

export const CatalogPage: React.FC<any> = () => {
  const products = useSelector(selectProductsListByCat);
  const categories = useSelector(selectCategories);
  const activeCat = useSelector(selectCategoriesActive);
  const total = useSelector(selectProductsTotal);
  const error = useSelector(selectProductsError);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts())
      /*.then(res => {
        alert('done')
      })*/
  }, [dispatch]);

  return <div>

    { error && <div className="alert alert-danger">errore server</div>}

    <ProductsForm
      products={products}
      categories={categories}
      onAddProduct={p => dispatch(addProduct(p))}
    />

    <hr/>

    {
      categories.map(c => {
        return <button
          className={cn({ 'bg-dark text-white': activeCat === c.id})}
          key={c.id} onClick={() => dispatch(changeActive(c.id))}>{c.name}</button>
      })
    }

    <ProductsList
      products={products}
      total={total}
      onDeleteProduct={(id) => dispatch(deleteProduct(id))}
      onToggleVisibility={p => dispatch(toggleProduct(p))}
    />

  </div>
};
