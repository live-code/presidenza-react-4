import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { decrement, increment, reset } from './store/counter.actions';
import { getCounter, getTotalPallet } from './store/counter.selectors';
import { setConfig } from '../../core/store/config.actions';
import { getItemsPerPallet, getMaterial } from '../../core/store/config.selectors';

export const CounterPage: React.VFC = () => {
  const dispatch = useDispatch();
  const counter = useSelector(getCounter)
  const totalPallets = useSelector(getTotalPallet)
  const material = useSelector(getMaterial)
  const itemsPerPallet = useSelector(getItemsPerPallet)

  return <div>
    <div>Ci sono {counter} products in {totalPallets} pallets</div>
    <div>Item Per Pallet: {itemsPerPallet}</div>
    <div>Material: {material}</div>

    <hr/>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(reset())}>reset</button>

    <hr/>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 4 }))}>set pallet to 4 items</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 6 }))}>set pallet to 6 items</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 12 }))}>set pallet to 12 items</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood' }))}>Wood</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic' }))}>Plastic</button>
  </div>
}
