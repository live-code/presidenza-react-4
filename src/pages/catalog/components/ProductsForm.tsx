import classnames from 'classnames';
import React, { useState } from 'react';
import { Product } from '../model/product';
import { addProduct } from '../store/products/products.actions';
import { Category } from '../model/category';

type FormState = Pick<Product, 'title' | 'price' | 'categoryID'>;

export interface ProductsFormProps {
  products: Product[];
  categories: Category[];
  onAddProduct: (product: FormState) => void;
}

export const ProductsForm: React.VFC<ProductsFormProps> = (props) => {
  const [value, setValue] = useState<FormState>({ title: '', price: 0, categoryID: 1});

  const onChangeHandler = (e: React.FormEvent<HTMLInputElement | HTMLSelectElement>) => {
    const isNumber = e.currentTarget.type === 'number';
    const isSelect = e.currentTarget.type === 'select-one';
    console.log(e.currentTarget.type)
    setValue({
      ...value,
      [e.currentTarget.name]: isNumber || isSelect ? +e.currentTarget.value : e.currentTarget.value
    })
  };


  const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    props.onAddProduct(value);
    setValue({ title: '', price: 0, categoryID: 1});
  };

  const isTitleValid = value.title.length > 3;
  const isPriceValid = value.price > 0;
  const isFormValid = isTitleValid && isPriceValid;

  return <div>
    <div>PRODUCT: {props.products.length}</div>

    <hr/>
    <div className="example">
      <form onSubmit={onSubmitHandler}>
        <input
          className={classnames(
            'form-control',
            { 'is-valid': isTitleValid },
            { 'is-invalid': !isTitleValid },
          )}
          type="text"
          placeholder="Write something..."
          onChange={onChangeHandler}
          value={value.title}
          name="title"
        />
        <input
          className={classnames(
            'form-control',
            { 'is-valid': isPriceValid },
            { 'is-invalid': !isPriceValid },
          )}
          type="number"
          placeholder="Write something..."
          onChange={onChangeHandler}
          value={value.price}
          name="price"
        />

        <select name="categoryID" onChange={onChangeHandler}>
          {/*<option value={-1}>Select Category</option>*/}
          {
            props.categories.map(c => {
              return <option key={c.id} value={c.id}>{c.name}</option>
            })
          }
        </select>
        <button disabled={!isFormValid} type="submit">SEND</button>
      </form>
    </div>
  </div>
};
