import React, { useEffect } from 'react';
import './App.css';
import { Navbar } from './core/components/NavBar';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HomePage } from './pages/home/HomePage';
import { UsersPage } from './pages/users/UsersPage';
import { CounterPage } from './pages/counter/CounterPage';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { Provider, useDispatch } from 'react-redux';
import { counterReducer } from './pages/counter/store/counter.reducer';
import { configReducer } from './core/store/config.reducer';
import { newsStore } from './pages/home/store/news.store';
import { usersReducer } from './pages/users/store/users.reducer';
import { catalogReducers } from './pages/catalog/store';
import { uiReducer } from './pages/users/store/ui.reducer';

const rootReducer = combineReducers({
  counter: counterReducer,
  config: configReducer,
  news: newsStore.reducer,
  users: usersReducer,
  ui: uiReducer,
  catalog: catalogReducers
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>


export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production'
})

function App() {


  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/counter">
            {/*<PageCounterContainer color="red"/>*/}
            <CounterPage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
