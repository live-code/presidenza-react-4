import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    list: [] as Product[],
    error: false
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.error = false;
      state.list = action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.list.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      state.error = false;
      const product = state.list.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    getProductsFailed(state) {state.error = true },
    addProductsFailed(state) {state.error = true },
    deleteProductsFailed(state) {state.error = true },
    toggleProductsFailed(state) {state.error = true },
  },
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess,
  getProductsFailed, addProductsFailed,
  deleteProductsFailed,
  toggleProductsFailed
} = productsStore.actions;
