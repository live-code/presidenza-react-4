import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getMaterial, getTheme } from '../store/config.selectors';
import cn from 'classnames';
import { loadTheme } from '../store/config.actions';

export const Navbar: React.FC = () => {
  const theme = useSelector(getTheme);
  console.log('render navbar')
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadTheme());
  }, [dispatch]);

  return (
    <nav
      className={cn(
        'navbar navbar-expand',
        {'navbar-light bg-light': theme === 'light'},
        {'navbar-dark bg-dark': theme === 'dark'},
      )}
    >
      <div className="navbar-brand">
        <NavLink
          activeClassName="active"
          className="nav-link"
          to="/">
          REDUX
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}
