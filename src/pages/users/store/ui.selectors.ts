import { RootState } from '../../../App';

export const getLeftPanelOpenStatus = (state: RootState) => state.ui.isLeftOpen;
export const getRightPanelOpenStatus = (state: RootState) => state.ui.isRightOpen;
