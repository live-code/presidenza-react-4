import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { User } from '../model/users';
import {
  addUserSuccess,
  deleteUserSuccess,
  loadUsersSuccess,
  setAsAdminSuccess
} from './users.actions';

const INITIAL_STATE: User[] = [];

export const usersReducer = createReducer(INITIAL_STATE, {
  [loadUsersSuccess.type]: (state, action: PayloadAction<User[]>) => {
    return [...action.payload]
  },
  [addUserSuccess.type]: (state, action: PayloadAction<User>) => {
    state.push(action.payload);
    // return [...state, action.payload]
  },
  [deleteUserSuccess.type]: (state, action: PayloadAction<number>) => {
    return state.filter(user => user.id !== action.payload)
  },
  [setAsAdminSuccess.type]: (state, action: PayloadAction<User>) => {
    let user = state.find(u => u.id === action.payload.id);
    if (user) {
      user.isAdmin = !user.isAdmin;
    }
    // return state.map(u => u.id === action.payload.id ? {...u, isAdmin: !u.isAdmin} : u)
  }
})
